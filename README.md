# RetroLinks
https://lachrymogenic.gitlab.io/retrolinks/archives/ <br />
![](https://lachrymogenic.github.io/retrolinksimages/icon.jpg) <br />
[![](https://lachrymogenic.github.io/retrolinksimages/for-retrostudio.svg)](https://www.roblox.com/games/5846386835/RetroStudio) <br />

## Summary
RetroLinks is a Hexo website, designed for archiving and collecting PlaceID's, titles, thumbnails, and other information.
The primary use case of RetroLinks is to find a specific place you want to join without having to scroll through pages at a snails pace. Wanna join "Work at a Pizza Place"? Click "Archives" and press CTRL + F and type in Pizza. I even have categories for certain places made in certain times like Early 2012 etc.

# Showcase
A showcase of benefits and the downsides of the current system that a website like this could negate.
### Search with RetroLinks: <br />
![](https://lachrymogenic.github.io/retrolinksimages/showcase.gif) <br />
### Avoid slow waiting times and "The 3 Page Per Minute Limit" <br />
![](https://lachrymogenic.github.io/retrolinksimages/showcase2.gif) <br />
![](https://lachrymogenic.github.io/retrolinksimages/showcase3.png) <br />

# How to Contribute
Contributing will be very helpful and important, the more people there are to add places, the more the list can grow and places will be easier to access. If you ever want to add your own place or a place, feel free to make a commit, and I will accept the commit if the information is correct, make sure you follow the format. You can mass upload .MD files with the Web IDE. Alternatively, you may also fork RetroLinks, if you would like to. 
## RetroScraper (Python Script)
Another way to contribute is to use my Scraper, made in Python. The scraper isn't perfect, as of now requires you to have a certain 1920 x 1030ish aspect ratio (Maximised Window on a 1080p monitor) and often gets the names of titles wrong. But its really good for PlaceID's which so far it hasn't seemed to ever get any PlaceID wrong. The Scraper fails whenever it cannot pick up Creator, Created or ID tags and places the file into a different folder for manual correction. If there is no ID, then you'll have to make the file manually.
## Fixing stuff up
If you see a Place with a jumbled or wrong information, (Example, a place with a title like: e38re y48r ekw) go to source/_posts/PLACEID.md and make a commit. Places with wrong information are due to the scraper accidentally getting some info wrong.
## Add your place
To add your place manually, go to source/_posts/ and create a new markdown file with your place's PlaceID. The format is as follows: <br />


    ---
    title: Example
    category: Late 2013
    thumbnail: https://lachrymogenic.github.io/retrolinksimages/PLACEID.jpg
    placeid: "PLACEID"
    ---
    ![](https://lachrymogenic.github.io/retrolinksimages/PLACEID.jpg)
    Creator: Creator's Username
    Created: Jan 1 2013
    Updated: 5 Months Ago (Doesn't really matter)
    Visits: 10000

    Time: Late 2013
    
    # ID: PLACEID
    
To add your thumbnail, go to https://github.com/Lachrymogenic/retrolinksimages and upload your place thumbnails as your PLACEID.jpg
